<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/blocs', 'BlocController@index');

Route::get('/add-bloc-form', 'BlocController@create');
Route::post('/add-bloc-form', 'BlocController@store');
Route::delete('/delete-bloc', 'BlocController@destroy');
Route::get('/update-bloc/{id}', 'BlocController@edit');
Route::patch('/update-bloc/{id}', 'BlocController@update');

Route::get('/classes', 'KlaseController@index');
Route::get('/add-class-form', 'KlaseController@create');
Route::post('/add-class-form', 'KlaseController@store');
Route::delete('/delete-class', 'KlaseController@destroy');
Route::get('/update-class/{id}', 'KlaseController@edit');
Route::patch('/update-class/{id}', 'KlaseController@update');
