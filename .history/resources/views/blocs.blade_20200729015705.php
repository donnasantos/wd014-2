@extends('layouts.template');
@section('title', 'Add Bloc Form');
@section('content')
    <h1 class="py-5 text-center">All Blocs</h1>
    <div class="text-center">
        <a href="/add-bloc-form" class="btn btn-primary my-2">Add bloc</a>
    </div>
@endsection