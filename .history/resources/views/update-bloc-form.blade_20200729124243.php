@extends('layouts.template');
@section('title', 'Update Bloc Form');
@section('content')
    <h1 class="py-5 text-center">Update Bloc Form</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/update-bloc/{{ $bloc->id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="name">Bloc Name</label>
                        <input type="text" name="name" class="form-control" value="{{ $bloc->name }}">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Update Bloc</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection