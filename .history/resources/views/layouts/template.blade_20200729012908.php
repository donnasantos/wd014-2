<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>@yield('title')</title>

{{--    bootswatch --}}
    <link rel="stylesheet" href="https://bootswatch.com/4/slate/bootstrap.css">
</head>
<body>
    
    @yield('content')

    <footer class="bg-primary">
        <div class="d-flex justify-content-center">
            <p class="my-5 text-white">Made with Love By B</p>
        </div>
    </footer>
</body>
</html>
