@extends('layouts.template');
@section('title', 'Add Bloc Form');
@section('content')
    <h1 class="py-5 text-center">Add Bloc Form</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-bloc-form" method="">
                    <div class="form-group">
                        <label for="name">Bloc Name</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Add Bloc</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection