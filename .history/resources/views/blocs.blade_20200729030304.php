@extends('layouts.template');
@section('title', 'Add Bloc Form');
@section('content')
    <h1 class="py-5 text-center">All Blocs</h1>
    <div class="text-center">
        <a href="/add-bloc-form" class="btn btn-primary my-2">Add bloc</a>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <table class="table-striped">
                    <thead>
                        <tr>
                            <th>Bloc ID</th>
                            <th>Bloc Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                      
                           @foreach()
                           @endforeach()
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection