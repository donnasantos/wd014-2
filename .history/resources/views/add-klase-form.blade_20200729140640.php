@extends('layouts.template')
@section('title', 'Add Bloc Form')
@section('content')
    <h1 class="py-5 text-center">Update Class Form</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/update-class/{{ $klase->id }}" method="POST">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="name">Class Name:</label>
                        <input type="text" name="name" class="form-control" value="{{ $klase->name }}">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-primary">Update Class</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
