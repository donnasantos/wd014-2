@extends('layouts.template')
@section('title', 'Blocs')
@section('content')
    <h1 class="text-center py-5">All Class</h1>
    <div class="text-center">
        <a href="/add-klase-form" class="btn btn-primary my-2">Add Class</a>
    </div>
    <div class="container">
            <div class="col-lg-6 offset-lg-3">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Class Id</th>
                            <th>Class Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($klases as $klase)
                             <tr>
                                <td>{{ $klase->id }}</td>
                                <td>{{  $klase->id }}</td>
                                <td>
                                    <form action="/delete-class" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="bloc_id" value="{{ }}">
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    <a href="/update-class/{{}}" class="btn btn-info">Update</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
@endsection
