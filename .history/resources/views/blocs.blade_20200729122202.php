@extends('layouts.template')
@section('title', 'Blocs')
@section('content')
    <h1 class="text-center py-5">All Blocs</h1>
    <div class="text-center">
        <a href="/add-bloc-form" class="btn btn-primary my-2">Add Bloc</a>
    </div>
    <div class="container">
            <div class="col-lg-6 offset-lg-3">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Bloc ID</th>
                            <th>Bloc Name</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($blocs as $bloc)
                            <tr>
                                <td>{{ $bloc->id }}</td>
                                <td>{{ $bloc->name }}</td>
                                <td>
                                    <form action="/delete-bloc" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <input type="hidden" name="bloc_id" value="{{ $bloc->id }}">
                                        <button type="submit" class="btn btn-danger">Delete</button>
                                    </form>
                                    <a href="/update-bloc/{{$block->id}}" class="btn btn-info">Update</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
    </div>
@endsection
