<?php

namespace App\Http\Controllers;

use App\Klase;
use Illuminate\Http\Request;

class KlaseController extends Controller
{

    public function index(){
        $klases = Klase::all();
        return view('klases', compact('klases'));
    }


    public function create(){
        return view('add-klase-form');
    }


    public function store(Request $request){
        $klase = new Klase;
        $klase->name = $request->name;
        $klase->save();
        return redirect('/classes');
    }


    public function show(Klase $klase){
        
    }


    public function edit($id){
        $klase = Klase::find($id);
        return view('update-klase-form', compact('klase'));
    }

  
    public function update(Request $request, $id){
        $klase = Klase::find($id);
        $klase->name = $request->name;
        $klase->save();
        return redirect('/classes');
    }


    public function destroy(Request $request){
        $id = $request->klase_id;
        $klase = Klase::find($id);
        $klase->delete();
        return redirect('/classes');
    }
}
