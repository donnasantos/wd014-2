<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\bloc;

class BlocController extends Controller
{
    public function index(){
        $blocs = Bloc::all();
        return view('blocs', compact('blocs'));
    }

    public function create(){
        return view('add-blocs');
    }

    public function store(Request $request){
        $new_bloc = new Bloc;
        $new_bloc->name = $request->name;
        $new_bloc->save();
        return redirect('/blocs');
    }

    public function destroy(Request $request){
        $id = $request->block_id;
        $bloc = Bloc::find(id);
        $bloc->delete();
        return redirect('/blocs');

    }
}
