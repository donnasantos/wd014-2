<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\bloc;

class BlocController extends Controller
{
    public function index(){
        return view('blocs');
    }

    public function create(){
        return view('add-blocs');
    }

    public function store(Request $request){
        $new_bloc = new Bloc;
        $new_bloc->name = $request->name;
        $new_bloc->save();
        return redirect ('/add-b')
    }
}
