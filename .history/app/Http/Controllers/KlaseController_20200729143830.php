<?php

namespace App\Http\Controllers;

use App\Klase;
use Illuminate\Http\Request;

class KlaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $klases = Klase::all();

        return view('klases', compact('klases'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add-klase-form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $klase = new Klase;
        $klase->name = $request->name;
        $klase->save();

        return redirect('/classes');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Klase  $klase
     * @return \Illuminate\Http\Response
     */
    public function show(Klase $klase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Klase  $klase
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $klase = Klase::find($id);

        return view('update-klase-form', compact('klase'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Klase  $klase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $klase = Klase::find($id);
        $klase->name = $request->name;
        $klase->save();

        return redirect('/classes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Klase  $klase
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->klase_id;
        $klase = Klase::find($id);
        $klase->delete();

        return redirect('/classes');
    }
}
